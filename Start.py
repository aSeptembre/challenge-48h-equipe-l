from binnary import Binary
from secureSys import *
from suiteMagik import suiteMagik
from steganographie import GaleriSecret
print("voici vos règle pour cette mission : \n"
      "- vous avez acces au code entier \n"
      "- vous pouvez modifié le code qui se situe dans le dossier casser \n"
      "- il y a un bonus dans chaque mission qui une fois rassemblé vous donne acces a une surprise \n"
      "- pour passer a la mission suivante vous devez imperativement nous validé le code reçu \n"
      "- vous n'avez pas le droit de dechiffrer les valeur qui ne vous sont pas donné \n")

print("[Première étape : séquence d’amorçages du missile] \n")
print("Pour amorcer le missile nucléaire il faut amorcer les barres de plutonium qu’il contient cependant il est primordial que l’IA amorce la dernière barre seule pour initialiser la séquence finale. Cependant L’IA a un bug, elle ne veut pas amarrer la dernière barre et la laisse toujours à l’utilisateur. Il faut que vous arrangez le code de tel sorte à ce que vous gagnez, c’est un code basé sur ce fameux jeux enfantin des allumettes.")
SecureSys()
print("[Seconde étape : séquence de désamarrage du missile] \n")
print("Bonjour dans cette etape vous devez trouvez la reponce a une question éxistanciel a vous de trouvez la question et la reponce :)")
suiteMagik()
print("[Troisième étape : séquence du sas à missile] \n")
print("Cette étape est relativement simple nous avons un code que l’on ne comprend pas cependant il nous faut comprendre a vous de retrouver le message et faire en sorte que la sécurité puisse le lire")
Binary()
print("[Quatrième étape : séquence de lancement du missile] \n")
print("Nous avons une liste d'images top secrètes cependant nous savons plus quelle image contient le bon message Mais ce n’est pas tout, vous devrez aussi décrypter le message que vous trouverez. la change devra être de votre côté pour cette épreuve vous en aurez besoin pour ne pas perdre trop de temps. Vite, le temps presse !")
GaleriSecret()
