import secrets
from PIL import Image
pasadditionele=5
def PRIVATE(n):

    chaineP = n
    chaineV=""
    code = 0
    i = 0
    while i < len(chaineP):
        myInt = secrets.randbelow(4)

        if myInt == 0:
            code = ord(chaineP[i])+20+pasadditionele
            chaineV=chaineV + "$" + chr(code)
        elif myInt == 1:
            code=ord(chaineP[i]) + 5+pasadditionele
            chaineV=chaineV+"£"+chr(code)
        elif myInt == 2:
            code = ord(chaineP[i])+16+pasadditionele
            chaineV=chaineV + "*" + chr(code)
        elif myInt == 3:
            code=ord(chaineP[i]) + 3+pasadditionele
            chaineV=chaineV+"%"+chr(code)
        i = i + 1

    return(chaineV)
def SEECRETT(chaine1):
    chaine2=""
    code=0
    i=0
    while i<len(chaine1):
        if chaine1[i] == "$":
            code = ord(chaine1[i+1]) - 20 -pasadditionele
            chaine2=chaine2 +chr(code)
        if chaine1[i] == "£":
            code = ord(chaine1[i+1]) - 5-pasadditionele
            chaine2 = chaine2 + chr(code)
        if chaine1[i] == "*":
            code = ord(chaine1[i+1]) - 16-pasadditionele
            chaine2 = chaine2 + chr(code)
        if chaine1[i] == "%":
            code = ord(chaine1[i+1]) - 3-pasadditionele
            chaine2 = chaine2 + chr(code)
        i = i+2
    return(chaine2)

def CONFIDENTIAL(str):

    chaine2=""
    code=0
    i=0
    while i<len(str):
        if str[i] == "$":
            code = ord(str[i+1]) - 22 -pasadditionele
            chaine2=chaine2 +chr(code)
        if str[i] == "£":
            code = ord(str[i+1]) - 7-pasadditionele
            chaine2 = chaine2 + chr(code)
        if str[i] == "*":
            code = ord(str[i+1]) - 18-pasadditionele
            chaine2 = chaine2 + chr(code)
        if str[i] == "%":
            code = ord(str[i+1]) - 7-pasadditionele
            chaine2 = chaine2 + chr(code)
        i = i+2
    return(chaine2)

def SECURITY(n): 
    chaineP = n
    chaineV=""
    code = 0
    i = 0
    while i < len(chaineP):
        myInt = secrets.randbelow(4)

        if myInt == 0:
            code = ord(chaineP[i])+22+pasadditionele
            chaineV=chaineV + "$" + chr(code)
        elif myInt == 1:
            code=ord(chaineP[i]) + 7+pasadditionele
            chaineV=chaineV+"£"+chr(code)
        elif myInt == 2:
            code = ord(chaineP[i])+18+pasadditionele
            chaineV=chaineV + "*" + chr(code)
        elif myInt == 3:
            code=ord(chaineP[i]) + 7+pasadditionele
            chaineV=chaineV+"%"+chr(code)
        i = i + 1

    return(chaineV)

def decode(src):
    im = Image.open(src)
    r, g, b = im.split()
    r = list(r.getdata())

    p = [str(x % 2) for x in r[0:8]]
    q = "".join(p)
    q = int(q, 2)

    n = [str(x % 2) for x in r[8:8 * (q + 1)]]
    b = "".join(n)
    message = ""
    for k in range(0, q):
       l = b[8 * k:8 * k + 8]
       message += chr(int(l, 2))

    return message

