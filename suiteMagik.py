# En quelle date est parue le premier ordinateur du monde?
# Septembre 1945 -> a: 5 b: 7 u0: 3 nMax: 15 epsilon: 0.5
# Février 1891 -> a: 5 b: 7 u0: 1 nMax: 11 epsilon: 2
# Mars 1952 -> a: 7 b: 5 u0: 3 nMax: 20 epsilon: 0.02
def suiteMagik():
    a = float(input("\n" "a : "))
    b = float(input("\n" "b : "))
    u0 = float(input("\n" "u0 : "))
    nMax = float(input("\n" "nMax : "))
    epsilon = float(input("\n" "epsilon : "))

    iteration = 0
    un = a + b / u0

    while abs(un - u0) > epsilon or iteration < nMax:
        var = un
        un = a + b / var
        u0 = var
        iteration = iteration + 1
    print("result -> ", un)

    if un == 6.140054944629241:
        print("Bravo vous avez désamarré du missile")
    else:
        suiteMagik()