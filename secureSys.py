from casser.SystemSecure import algo

def Play(nbBarre):
    nbRetire = int(input("il reste " + str(nbBarre) + " vous en retirez combien ? (entre 1 et 3) :"))
    if nbRetire > 3 or nbRetire < 1:
        Play(nbBarre)
    else:
        return nbBarre-nbRetire
        
def SecureSys():
    nbBarre = 12
    uturn = False
    while nbBarre > 0:
        if uturn == False:
            nbRetire = algo(nbBarre)
            if nbRetire >= 4 or nbRetire <= 0:
                print("bad input")
            else:
                nbBarre = nbBarre - nbRetire
                uturn = True
        else:
            nbBarre = Play(nbBarre)
            uturn = False
    if uturn == True:
        print("Bravo")
    else:
        print("you Lose Try again")
        SecureSys()
